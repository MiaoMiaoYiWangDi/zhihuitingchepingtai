# zhihuitingchepingtai

#### 介绍
本项目实现智慧停车场功能，车位查询，自助付费，后台查询，计时与超时告警，实现无人化，无纸化，车来即停；车走付费；解决场景包括场内停车场、场外停车场。自助会员服务以及友善的和其他平台对接。对接智能设备包括海康威视、科拓、华夏等国内主流设备厂商，且可快速接入小众厂商设备。

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/113924_35fca6d3_798614.png "屏幕截图.png")<br>

#### 功能说明
1.  **设备硬件管理** <br>
支持快速接入市面主流的海康威视，科拓，臻实等不同的厂商设备，快速解析处理设备返回数据。车辆计费，开闸关闸进出无人化，车辆信息自动储存管理。实时监控设备状态，设备下线可提现停车场管理员及时处理。
2.  **停车场管理** <br>
十分强大的车场信息管理，您可以配置多个停车场名称，负责人，泊位数，GPS坐标等基本信息，也可以配置多种收费规则，收费模式，停车场特殊车辆类型，闸门开关机制信息
3.  **人员管理** <br>
可建立分配多角色，多平台权限。如停车场人员，物业人员，商家人员等等。不同角色拥有不同后台管理权限。完美支持小区停车场，商家合作停车场等多种使用场景，可按照不同需求充分适配人员管理，极大的提高了运营效率
4.  **套餐管理** <br>
停车场可以自行添加多种时间金额的月卡套餐，车位套餐，天卡/周卡/月卡完全自定义，定时过期提醒用户到期续费。
5.  **计费规则** <br>
高度自定义化的停车收费规则，支持起步价，顶额支付，按天按小时定制计费，针对特别车辆的计费模式。可以满足绝大部分的车辆计费场景
7.  **多端远程管理** <br>
支持PC端，APP端远程监控操作车场设备硬件。可及时处理停车场突发意外场景。
8.  **附近停车场** <br>
小程序支持地图查看附近车场位置，还可以点击查看相应停车场的收费价格。提高用户体验，吸引顾客消费
9.  **服务商模式** <br>
可配置添加收益多个分成方，快捷向合作伙伴支付分成。支持微信，支付宝分账。适应更复杂的实际应用场景。详细记录分账支付记录。查账轻松，安全稳定。
10.  **收费选择** <br>
支持微信，支付宝，ETC付款消费，可自由选择


#### 特别说明
1.  此项目系公司产品，如需接入请联系公司。
2.  此项目可交付源代码
3.  此项目需要收费
4.  如果需求请联系管理员 QQ1352756907  微信15290819383
5.  项目详细说明文档地址：http://resource.zzdingyuan.top/%E6%99%BA%E6%85%A7%E5%81%9C%E8%BD%A6%E7%AE%A1%E7%90%86%E5%B9%B3%E5%8F%B0.pdf
6.  后台管理体验地址：http://tingche.dingyuankeji.shop/index.html   账号：admin/密码：li123456 请勿修改管理员账户密码，影响他人体验
7.  微信小程序体验码<br>
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F%E7%A0%81.jpg)<br>


#### 图片示例
## $\color{orange}{小程序主要功能一览}$<br>
### 首页信息
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163136.jpg" width="500" height="1000"/><br>
### 我的车辆
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163144.jpg" width="500" height="1000"/><br>
### 地图查找
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163222.jpg" width="500" height="1000"/><br>
### 个人主页
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163229.jpg" width="500" height="1000"/><br>
### 商家优免中心
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163300.jpg" width="500" height="1000"/><br>
### 菜单中心
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163306.jpg" width="500" height="1000"/><br>
### 发票申请
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163311.jpg" width="500" height="1000"/><br>
### 套餐列表
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163324.jpg" width="500" height="1000"/><br>
### 支付页面
<img src="https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/Screenshot_20220527_163330.jpg" width="500" height="1000"/><br>


## $\color{orange}{后台管理功能一览}$<br>
### 数据大屏
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E6%95%B0%E6%8D%AE%E5%A4%A7%E5%B1%8F.png)<br>
### 后台管理首页
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86%E9%A6%96%E9%A1%B5.png)<br>
### 后台车辆类型管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E8%BD%A6%E8%BE%86%E7%B1%BB%E5%9E%8B%E7%AE%A1%E7%90%86.png)<br>
### 后台订单管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E8%AE%A2%E5%8D%95%E7%AE%A1%E7%90%86.png)<br>
### 后台岗亭监控管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E5%B2%97%E4%BA%AD%E7%9B%91%E6%8E%A7%E7%AE%A1%E7%90%86.png)<br>
### 后台人员管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E4%BA%BA%E5%91%98%E7%AE%A1%E7%90%86.png)<br>
### 后台设备管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E8%AE%BE%E5%A4%87%E7%AE%A1%E7%90%86.png)<br>
### 后台审核管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E5%AE%A1%E6%A0%B8%E7%AE%A1%E7%90%86.png)<br>
### 后台停车记录管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E5%81%9C%E8%BD%A6%E8%AE%B0%E5%BD%95%E7%AE%A1%E7%90%86.png)<br>
### 后台物业管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E7%89%A9%E4%B8%9A%E7%AE%A1%E7%90%86.png)<br>
### 后台流水管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E7%89%A9%E4%B8%9A%E6%B5%81%E6%B0%B4.png)<br>
### 后台系统管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E7%B3%BB%E7%BB%9F%E7%AE%A1%E7%90%86.png)<br>
### 后台套餐管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E5%A5%97%E9%A4%90%E7%AE%A1%E7%90%86.png)<br>
### 后台停车场管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E5%81%9C%E8%BD%A6%E5%9C%BA%E7%AE%A1%E7%90%86.png)<br>
### 后台计费规则管理管理
![](https://gitee.com/Bluebear118/images3/raw/master/zhihuitingche/%E5%90%8E%E5%8F%B0%E8%BD%A6%E5%9C%BA%E8%AE%A1%E8%B4%B9%E8%A7%84%E5%88%99%E8%AE%BE%E5%AE%9A.png)<br>

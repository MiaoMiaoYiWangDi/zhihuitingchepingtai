# zhihuitingchepingtai

#### Description
本项目实现智慧停车场功能，车位查询，自助付费，后台查询，计时与超时告警，实现无人化，无纸化，车来即停；车走付费；解决场景包括场内停车场、场外停车场。自助会员服务以及友善的和其他平台对接。对接智能设备包括海康威视、科拓、华夏等国内主流设备厂商，且可快速接入小众厂商设备。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
